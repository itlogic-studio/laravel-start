<?php

use Illuminate\Support\Facades\Config;

return array(

    'enabled' => !Config::get('app.debug'),

    // If you are using a javascript framework that conflicts
    // with Blade's tags, you can change them here
    'blade' => array(
        'contentTags' => array('{{', '}}'),
        'escapedContentTags' => array('{{{', '}}}')
    )

);
