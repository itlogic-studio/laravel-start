## Laravel PHP Framework working assembly (4.2)

###Senrty 2.1
A modern and framework agnostic authorization and authentication package featuring groups, permissions, custom hashing algorithms and additional security features.
`artisan migrate --package=cartalyst/sentry`

###PHP Debugbar integration for Laravel
This is a package to integrate PHP Debug Bar with Laravel. It includes a ServiceProvider to register the debugbar and attach it to the output.

###Laravel IDE Helper Generator
This packages generates a file that your IDE can understand, so it can provide accurate autocompletion.
`artisan ide-helper:generate`

###Fast Workflow in Laravel With Custom Generators
This Laravel 4 package provides a variety of generators to speed up your development process. These generators include:

* generate:model
* generate:view
* generate:controller
* generate:seed
* generate:migration
* generate:pivot
* generate:resource
* generate:scaffold

###Presenter
This library provides a simple class to help make a Presenter for your objects or arrays. It also has little extras for use within Laravel with minimal extra code in your controllers (in most cases no extra code)

###Ardent
Self-validating smart models for Laravel Framework 4's Eloquent O/RM.

###AWS Service Provider
A simple Laravel 4 service provider for including the AWS SDK for PHP.

###Laravel HTML Minify
This package compresses the HTML output from your Laravel 4 application, seamlessly reducing the overall response size of your pages.

###Faker PHP library
Faker is a PHP library that generates fake data for you. Whether you need to bootstrap your database, create good-looking XML documents, fill-in your persistence to stress test it, or anonymize data taken from a production service, Faker is for you.

### Bower, GruntJS
Base config grunt and bower