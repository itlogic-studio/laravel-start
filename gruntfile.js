module.exports = function(grunt) {
    grunt.initConfig({
        // Отслеживаем изменение в файлах
        watch: {
            // Скрипты
            scripts: {
                files: ['public/assets/js-app/*.js'],
                tasks: ['concat:main', 'uglify'],
                options: {
                    spawn: false
                }
            },
            // Стили
            styles: {
                files: [
                    'public/assets/less/core.less',
                    'public/assets/less/style.less'
                ],
                tasks: ['less'],
                options: {
                    spawn: false
                }
            }

        },

        // Склеиваем JS
        concat: {
            main: {
                src: [
                    'public/assets/js-app/*.js'
                ],
                dest: 'public/assets/js-app/build/app.js'
            },
            lib: {
                src: [
                    'public/assets/components/angular/angular.min.js',
                    'public/assets/components/angular-animate/angular-animate.min.js',
                    'public/assets/components/angular-resource/angular-resource.min.js',

                    'public/assets/components/angular-bootstrap/ui-bootstrap.min.js',
                    'public/assets/components/angular-bootstrap/ui-bootstrap-tpls.min.js',

                    'public/assets/components/angularjs-utilities/src/directives/rcSubmit.js',
                    'public/assets/components/angularjs-utilities/src/modules/rcDisabled.js',
                    'public/assets/components/angularjs-utilities/src/modules/rcForm.js',

                    'public/assets/components/angular-ui-utils/jq.min.js',

                    'public/assets/components/slimScroll/jquery.slimscroll.min.js'
                ],
                dest: 'public/assets/js/components.min.js'
            }

        },

        // Сжимаем JS
        uglify: {
            options: {
                //banner: '/* www.itlogic-group.ru (Compiled: <%= grunt.template.today("dd.mm.yyyy") %>) */',
                sourceMap: true
            },
            main: {
                files: {
                    'public/assets/js-app/build/app.min.js': '<%= concat.main.dest %>'
                }
            }
        },
        // Компилируем less
        less: {
            production: {
                options: {
                    cleancss: true
                },
                files: {
                    "public/assets/css/core.css" : "public/assets/less/core.less",
                    "public/assets/css/style.css": "public/assets/less/style.less"
                }
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            files: 'public/assets/js-app/users/*.js'
        }
    });

    // Загрузка плагинов
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Задача по умолчанию
    grunt.registerTask('default', ['concat', 'uglify', 'less']);
};